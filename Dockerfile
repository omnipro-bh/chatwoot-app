FROM cloudron/base:4.0.0@sha256:31b195ed0662bdb06a6e8a5ddbedb6f191ce92e8bee04c03fb02dd4e9d0286df

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# Without the old ssl package, cert verification in slack plugin fails
# https://github.com/chatwoot/chatwoot/issues/6377 https://github.com/slack-ruby/slack-ruby-client/issues/422
RUN wget http://archive.ubuntu.com/ubuntu/pool/main/o/openssl/{libssl1.1_1.1.1f,libssl-dev_1.1.1f,openssl_1.1.1f}-1ubuntu2_amd64.deb && \
    dpkg -i {libssl1.1_1.1.1f,libssl-dev_1.1.1f,openssl_1.1.1f}-1ubuntu2_amd64.deb && \
    rm {libssl1.1_1.1.1f,libssl-dev_1.1.1f,openssl_1.1.1f}-1ubuntu2_amd64.deb

RUN apt-get update && \
    apt install -y libvips && \
    rm -rf /var/cache/apt /var/lib/apt/lists

# install rbenv since we need ruby 3.0.2
RUN mkdir -p /usr/local/rbenv && curl -LSs "https://github.com/rbenv/rbenv/archive/refs/tags/v1.2.0.tar.gz" | tar -xz -C /usr/local/rbenv --strip-components 1 -f -
ENV PATH /usr/local/rbenv/bin:$PATH
RUN mkdir -p "$(rbenv root)"/plugins/ruby-build && curl -LSs "https://github.com/rbenv/ruby-build/archive/refs/tags/v20230512.tar.gz" | tar -xz -C "$(rbenv root)"/plugins/ruby-build --strip-components 1 -f -

# install specific ruby version
ARG RUBY_VERSION=3.2.2
RUN rbenv install ${RUBY_VERSION}
ENV PATH /root/.rbenv/versions/${RUBY_VERSION}/bin:$PATH

# install specific bundler version
ARG BUNDLER_VERSION=2.3.26
RUN gem install bundler -v ${BUNDLER_VERSION}

ARG CHATWOOT_VERSION=3.0.0
RUN curl -LSs "https://github.com/chatwoot/chatwoot/archive/refs/tags/v${CHATWOOT_VERSION}.tar.gz" | tar -xz -C /app/code/ --strip-components 1 -f -

ENV RAILS_ENV "production"
ENV EXECJS_RUNTIME "Disabled"
ENV BUNDLE_WITHOUT "development:test"
ENV RAILS_SERVE_STATIC_FILES "true"
ENV INSTALLATION_ENV="cloudron"

# due to openssl issues we need node 16
ARG NODE_VERSION=16.18.1
RUN mkdir -p /usr/local/node-${NODE_VERSION} && \
    curl -L https://nodejs.org/dist/v${NODE_VERSION}/node-v${NODE_VERSION}-linux-x64.tar.gz | tar zxf - --strip-components 1 -C /usr/local/node-${NODE_VERSION}
ENV PATH /usr/local/node-${NODE_VERSION}/bin:$PATH

RUN bundle install -j 4 -r 3 && \
    yarn install

# create prod assets
RUN SECRET_KEY_BASE=precompile_placeholder RAILS_LOG_TO_STDOUT=enabled bundle exec rake assets:precompile && rm -rf spec node_modules tmp/cache;

RUN ln -s /app/data/env /app/code/.env && \
    rm -rf /app/code/storage && ln -s /app/data/storage /app/code/storage && \
    rm -rf /app/code/tmp     && ln -s /tmp /app/code/tmp

# add supervisor configs
RUN sed -e 's,^logfile=.*$,logfile=/run/supervisord.log,' -i /etc/supervisor/supervisord.conf
COPY supervisor/* /etc/supervisor/conf.d/

COPY env.sh.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
