#!/usr/bin/env node

/* jshint esversion: 8 */
/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = 'test';
    const TEST_TIMEOUT = 10000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const DISPLAY_NAME = 'test cloudron';
    const COMPANY = 'Cloudron';
    const EMAIL = 'test@cloudron.local';
    const PASSWORD = 'Changeme?123';

    var browser, app;

    before(function () {
        const options = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.HEADLESS) options.addArguments('headless');

        browser = new Builder().forBrowser('chrome').setChromeOptions(options).build();
    });

    after(function () {
        browser.quit();
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function createAccount() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.id('user_name'));
        await browser.findElement(By.id('user_name')).sendKeys(DISPLAY_NAME);
        await browser.findElement(By.id('user_company')).sendKeys(COMPANY);
        await browser.findElement(By.id('user_email')).sendKeys(EMAIL);
        await browser.findElement(By.id('user_password')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@type="submit"]')).click();
        await waitForElement(By.xpath('//h2[contains(text(), "Login to Chatwoot")]'));
    }

    async function login() {
        await browser.get(`https://${app.fqdn}/app/login`);
        await waitForElement(By.id('email_address'));
        await browser.findElement(By.id('email_address')).sendKeys(EMAIL);
        await browser.findElement(By.id('password')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@data-testid="submit_button"]')).click();
        await waitForElement(By.xpath('//div[contains(text(), "TC")]'));
    }

    async function loginOld() {
        await browser.get(`https://${app.fqdn}/app/login`);
        await waitForElement(By.xpath('//input[@data-testid="email_input"]'));
        await browser.findElement(By.xpath('//input[@data-testid="email_input"]')).sendKeys(EMAIL);
        await browser.findElement(By.xpath('//input[@data-testid="password_input"]')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//button[@data-testid="submit_button"]')).click();
        await waitForElement(By.xpath('//div[contains(text(), "TC")]'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//div[contains(text(), "TC")]'));
        await browser.findElement(By.xpath('//div[contains(text(), "TC")]')).click();
        await waitForElement(By.xpath('//span[contains(text(), "Logout")]'));
        await browser.findElement(By.xpath('//span[contains(text(), "Logout")]')).click();
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can create account', createAccount);
    it('can login', login);
    it('can logout', logout);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`); });

    it('can login', login);
    it('can logout', logout);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('can logout', logout);

    it('move to different location', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync(`cloudron install --appstore-id com.chatwoot.cloudronapp --location ${LOCATION}`, EXEC_ARGS); });
    it('can get app information', getAppInfo);

    it('can create account', createAccount);
    it('can login', loginOld);
    it('can logout', logout);

    it('can update', function () { execSync(`cloudron update --app ${app.id}`, EXEC_ARGS); });

    it('can login', login);
    it('can logout', logout);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
    });
});
