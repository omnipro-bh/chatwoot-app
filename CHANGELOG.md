[0.1.0]
* initial version with Chatwoot 1.21.1

[0.2.0]
* Fix image uploads

[0.3.0]
* Fix transactional emails

[1.0.0]
* First stable package release based on Chatwoot 1.21.1

[1.1.0]
* Update Chatwoot to 1.22.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v1.22.0)
* Chatwoot command bar for improved productivity
* Ability to define and add custom attributes to conversations/contacts
* Ability to add notes to contact on the CRM page
* Ability to configure Dialogflow integration across all channel types except email
* Ability to rearrange the conversation sidebar
* Ability to specify subject to outgoing emails

[1.1.1]
* Update Chatwoot to 1.22.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v1.22.1)
* Fix the issue of agent messages leaking into unverified contact sessions having the same contact email
* Added indication in UI for unverified contact sessions

[1.2.0]
* Update Chatwoot to 2.0.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.0.0)
* IMAP and SMTP support for email channels
* Dedicated View for conversation mentions
* Advanced conversation filters
* Advanced contact filters
* Template support for 360 dialog WhatsApp channels
* List and Checkbox support for custom attributes
* View conversation summary in notification emails
* OpenAPI compliant swagger docs
* Numerous bug fixes and enhancements

[1.2.1]
* Update to base image v3.1.0
* Fix .env file loading

[1.2.2]
* Refactor env file so it can be sourced to run rails commands

[1.2.3]
* Update Chatwoot to 2.0.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.0.1)
* Fixed memory leak with email parsing
* Attachment Upload improvements

[1.2.4]
* Update Chatwoot to 2.0.2
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.0.2)
* Issues with conversation assignment notifications

[1.2.5]
* Update Chatwoot to 2.1.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.1.0)
* Ability to disable messages in resolved Live chat conversations
* Support more filetypes as message attachments
* Ability to disable tweet conversation in the Twitter inbox
* Improved email confirmation flow for user accounts
* Improved email parsing
* Improved background job processing
* New Language: Slovak(sk)

[1.2.6]
* Update Chatwoot to 2.1.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.1.1)
* Fixing the delayed assignment notifications reported in some instances

[1.3.0]
* Update Chatwoot to 2.2.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.2.0)
* Automations ( Beta )
* Custom Views for conversations page
* Custom Filters for contacts page
* Email Signatures
* Support Bandwidth as a provider for SMS channel
* Ability to configure active storage direct uploads
* Ability to send multiple attachments from the agent dashboard
* Super Admin dashboard enhancements
* Support for interactive messages in Whatsapp channel ( 360 Dialog )
* Ability to group report data by weekly, monthly and yearly filters
* Ability to toggle conversation continuity via email for website channel
* Resolve action for Dialogflow integration
* SSL Verify mode and email encryption settings for SMTP mailboxes
* Vue-router for widget route management
* Numerous bug fixes and enhancements

[1.3.1]
* Update Chatwoot to 2.2.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.2.1)
* Fix for email messages without body
* Reporting improvements to consider timezone
* Fix for email signature being sent until it is unset in the editor
* Fix for nokogiri security vulnerability

[1.4.0]
* Update Chatwoot to 2.3.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.3.0)
* Timezone support in reports
* Ability for contacts to end conversations on chatwidget
* Ability to record and send voice notes from the agent dashboard
* Support for Hcaptcha in public forms
* Support for Instagram story mentions
* API to bulk update conversations
* Agent filter in CSAT reports
* Ability to configure 24-hour slots in business hours
* Support for MMS in Bandwidth SMS channel
* Ability to pass specific account ids when generating SSO URLs
* Display trends in reporting metrics
* Numerous bug fixes and enhancements

[1.4.1]
* Update Chatwoot to 2.3.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.3.1)
* Improves performance of agent endpoints

[1.4.2]
* Update Chatwoot to 2.3.2
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.3.2)
* fix for the invalid contact resolved conversation activity message
* fix for file copy-paste issue
* fix for the pre-chat form being rendered for identified contacts
* fix for auto-scroll not working in the dashboard
* sentry error fixes and improvements

[1.4.3]
* Update Chatwoot to 2.4.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.4.0)
* Notification view with real-time indicators
* Dark mode for the website widget
* Reports business hours
* IMAP SMTP improvements to support more providers
* Rich editor and cc support for new conversations
* Automation improvements
* Round robin assignment support for teams
* SDK methods for the pop-out chat widget
* ee edition docker images
* Numerous bug fixes and enhancements

[1.4.4]
* Update Chatwoot to 2.4.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.4.1)
* Fixing the accidentally merged conditions for open all day and closed all day.

[1.5.0]
* Update Chatwoot to 2.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.5.0)

[1.6.0]
* Update Chatwoot to 2.6.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.6.0)

[1.7.0]
* Update Chatwoot to 2.7.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.7.0)
* Support for Whatsapp Cloud API
* Ability to create dashboard apps from UI
* New Chatwoot CLI tool
* Support for Twilio Messaging Services
* Support HMAC enforcement in API channel
* Enhanced contact identification and merge logic
* APIs for custom sort
* API for updating contact avatar
* Numerous bug fixes and enhancements

[1.8.0]
* Update Chatwoot to 2.8.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.8.0)
* Email layout with improved email rendering
* Context Menu for conversations
* Chat Widget Builder
* CRUD APIs for macros
* Agent Capacity Config (enterprise)
* Ability to disable Gravatars
* Support for Elastic APM
* New Langauge: Thai
* Numerous API enhancements and bug fixes

[1.8.1]
* Update Chatwoot to 2.8.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.8.1)

[1.9.0]
* Update Chatwoot to 2.9.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.9.0)
* Help Center (Beta)
* Email conversation continuity for API channels
* Ability for Dashboard App Frames to query the information
* Improved blocking & throttling of abusive requests
* Automations supporting case insensitive filters
* Numerous API enhancements and bug fixes

[1.9.1]
* Update Chatwoot to 2.9.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.9.1)
* Minor help center related fixes

[1.10.0]
* Update Chatwoot to 2.10.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.10.0)
* Support for draft replies
* Ability to customise keymapping for the send reply button
* Ability to create canned responses from the dashboard
* Improvements to the Help Center module
* Performance improvements

[1.11.0]
* Update Chatwoot to 2.11.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.11.0)
* Macros
* Ability to search in help centre public portals
* Support for location messages in the WhatsApp channel
* Support for custom attributes in automation
* Ability to paste files/images from the Clipboard
* Realtime update for conversation attributes on agent dashboard
* Redis connection performance improvements
* Numerous other enhancements and bug fixes

[1.12.0]
* Add support for custom domains in helpdesk feature

[1.13.0]
* Update Chatwoot to 2.12.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.12.0)
* Message Delivery and read status for Whatsapp Channel
* Ability to view unattended conversations
* Ability to create and configure agent bots from UI
* Ability to mark a conversation as unread
* Ability to lock conversations to a single thread for supported channels
* Ability to use postfix as the mailer in self-hosted installations
* Ability to search in emoji selector
* Allow wildcard URLs in campaigns
* Allow users to disable marking offline automatically
* Numerous bug fixes and enhancements
* New languages: Latvian

[1.13.1]
* Update Chatwoot to 2.12.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.12.1)
* Fix the issue with WhatsApp voice recorder recording in WebM format.
* Fix the issue with WhatsApp's unread message count
* Add an option to select the audio alert tone
* Add an option to set the alert for every 30s unless the conversation is read

[1.14.0]
* Update Chatwoot to 2.13.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.13.0)
* Ability to do video calls on Chatwoot through the Dyte Integration
* Native support for Microsoft as a provider in Email Inboxes
* Ability to use template variables while messaging
* Ability to send longer audio messages on Whatsapp Channel
* Support for incoming location messages in Telegram Channel
* Numerous bug fixes and enhancements

[1.14.1]
* Update Chatwoot to 2.13.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.13.1)

[1.15.0]
* Update Chatwoot to 2.14.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.14.0)
* Conversation Participants
* Support for authentication via Google Oauth
* Google Translate Integration
* Image support in help centre articles
* UI for selecting Template variables
* Support contact attachments in Whatsapp Channel
* Support sending multiple attachments in Whatsapp Channel
* Ability to impersonate a user from Super Admin
* Ability to update Whatsapp Channel API key from UI
* Instance health status in Super Admin
* New Languages : Icelandic(is)
* Numerous bug fixes and enhancements

[1.15.1]
* Update to base image version 4

[1.16.0]
* Update Chatwoot to 2.15.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.15.0)
* Conversation Heatmap reports
* New Search experience
* RTL language support in Chatwoot Dashboard
* Audit Log APIs (EE edition)
* Support Rich messages in pre-chat form
* Support for WhatsApp messages from older Brazilian phone numbers
* Push notification support for safari
* Integration with Logrocket
* Numerous bug fixes and enhancements

[1.16.1]
* Migrate `/app/data/env` to `/app/data/env.sh`

[1.16.2]
* Make it easier to run bundle rails console

[1.17.0]
* Update Chatwoot to 2.16.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/v2.16.0)
* Context Menu for messages ( while right-clicking )
* Ability for admins to edit agent availability status
* Ability to link to a specific message
* Support for message-updated events in the telegram channel
* Ability to download heatmap reports
* support routing in the email channel using ex-original-to in the email header
* Additional automation filters ( email and phone number based )

[1.17.1]
* Update Chatwoot to 2.16.1
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.16.1)
* Fixes the issue with the UI when name of the customer is not present.

[1.17.2]
* Update Chatwoot to 2.17.1
* Update Ruby to 3.2.2
* Requires Redis 7 and Cloudron 7.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.17.0)
* OpenAI integration : ( Reply suggestions, summarization, and ability to improve drafts )
* Priority field for conversations
* Ability to sort conversations
* Instagram story replies show the original story
* SLA APIs ( Enterprise Edition )
* Improved email fetching for IMAP inboxes
* Support template variables in channel greeting messages
* Ability to create uncategorized help center articles
* Automatic reconnects in the background
* Numerous bug fixes and performance enhancements
* New Languages: Hebrew
* Fixes issues with avatars in docker images
* Fixes issues with pt-br translations

[1.18.0]
* Update Chatwoot to 2.17.1
* Update Ruby to 3.2.2
* Requires Redis 7 and Cloudron 7.5.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.17.0)
* OpenAI integration : ( Reply suggestions, summarization, and ability to improve drafts )
* Priority field for conversations
* Ability to sort conversations
* Instagram story replies show the original story
* SLA APIs ( Enterprise Edition )
* Improved email fetching for IMAP inboxes
* Support template variables in channel greeting messages
* Ability to create uncategorized help center articles
* Automatic reconnects in the background
* Numerous bug fixes and performance enhancements
* New Languages: Hebrew
* Fixes issues with avatars in docker images
* Fixes issues with pt-br translations

[1.18.1]
* Update Chatwoot to 2.18.0
* [Full changelog](https://github.com/chatwoot/chatwoot/releases/tag/2.18.0)
* New attachment view for conversations
* Ability to export contacts
* Support for interactive messages in WhatsApp ( button and list )
* Ability to update bot/user avatars from Super Admin Dashboard
* Audit logs for sign_in, sign_out, team events (EE)
* Support for configuring subdomains in widget SDK
* Enhanced filters for CSAT responses
* Support for super script tag in help center articles
* Numerous bug fixes and enhancements

[1.18.2]
* Add missing libvips

